# ResourceFramework

Load and save resources and data files

---

This plugin allows developers to easily and quickly get its plugin's resource files, as well as save its own custom data
in its plugin folder.

## For server owners

Put the JAR in `$SERVER/plugins` and it will work if any plugins require it.

## For developers

Javadoc is available at [src/main/javadoc](src/main/javadoc).

There are three main operations: loading a resource, loading data and saving data. Each operation has a method that
returns/takes an InputStream, and one that returns/takes a String.

Resource operations load data from the JAR itself, like the usual `Plugin#getResource` method.

Data operations occur within the plugin's data folder, which can be found by using `Plugin#getDataFolder`.

Use the `ResourceFramework#resourceFramework` static method to get an instance of the plugin. You can use the
`ResourceFramework#PluginToken` helper class to not specify the plugin for every method.

```java
// ...

import me.aecsocket.resourceframework.ResourceFramework.PluginToken;

// ...

public void onEnable() {
    PluginToken token = new PluginToken(this);

    String myResource = token.getStringResource("myResourceFile.txt");
    InputStream myData = token.getData("directory", "myData");
    token.saveStringData("Info here", false, "directory", "myData");
}
```

### Index

An index JSON file can be used to specify file paths to all resources, allowing you to easily edit and map resources.
You can do this by creating an `index.json` file in your root JAR folder, with the format:

```json
{
  "resource1": [
    [ "file", "path", "as", "a", "json", "array" ],
    [ "another", "file path" ]
  ],
  "items": [
    [ "items", "swords.json" ],
    [ "items", "bows.csv" ],
    [ "items", "food", "vegetables.yml" ]
  ],
  "sounds": [
    [ "sounds", "attack_sounds.json" ],
    [ "main_sounds.txt" ]
  ]
}
```

To get a plugin's `Index` object (which lists all `IndexEntries`), you can use `ResourceFramework#getIndex(Plugin)` or
its `PluginToken` counterpart.

To use an index, you can use `Index#getEntry(String)` to get an `IndexEntry` with a specified key (e.g. `resource1`,
`items`, `sounds`) and from that get a `List<InputStream>` or `List<String>` of all resources under that key. Resources
that do not exist will be `null`.

## Download

Currently there is no JAR download available. You must compile it yourself.