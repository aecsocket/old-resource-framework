package me.aecsocket.resourceframework.index;

import java.util.HashMap;
import java.util.Map;

/** An index of defined resources. */
public class Index {
    /** The path to the index file. */
    public static final String INDEX_PATH = "index.json";

    /** A {@link Map} of all {@link IndexEntry} objects. */
    private Map<String, IndexEntry> entries;

    public Index(Map<String, IndexEntry> entries) {
        this.entries = entries;
    }

    public Index() {
        this.entries = new HashMap<>();
    }

    /** Gets a {@link Map} of all {@link IndexEntry} objects.
     * @return A {@link Map} of all {@link IndexEntry} objects.
     */
    public Map<String, IndexEntry> getEntries() { return entries; }

    /** Gets an {@link IndexEntry} from a {@link String} key.
     * @param key The {@link String} key.
     * @return The {@link IndexEntry}.
     */
    public IndexEntry get(String key) { return entries.get(key); }

    /** Adds or updates an {@link IndexEntry} with a {@link String} key.
     * @param key The {@link String} key.
     * @param entry The {@link IndexEntry}.
     */
    public void put(String key, IndexEntry entry) { entries.put(key, entry); }

    /** Returns if an {@link IndexEntry} with a specified {@link String} key exists.
     * @param key The {@link String} key.
     * @return If the {@link IndexEntry} exists or not.
     */
    public boolean has(String key) { return entries.containsKey(key); }

    @Override
    public String toString() {
        return entries.toString();
    }
}
