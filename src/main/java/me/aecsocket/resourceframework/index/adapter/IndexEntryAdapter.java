package me.aecsocket.resourceframework.index.adapter;

import com.google.gson.*;
import me.aecsocket.pluginutils.json.JsonAdapter;
import me.aecsocket.pluginutils.json.JsonUtils;
import me.aecsocket.resourceframework.index.IndexEntry;
import org.bukkit.plugin.Plugin;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IndexEntryAdapter implements JsonAdapter<IndexEntry> {
    @Override
    public JsonElement serialize(IndexEntry object, Type type, JsonSerializationContext context) {
        JsonArray array = new JsonArray();
        for (String[] path : object.getPaths()) {
            JsonArray subArray = new JsonArray();
            for (String subPath : path)
                subArray.add(subPath);
            array.add(subArray);
        }
        return array;
    }

    @Override
    public IndexEntry deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonArray array = JsonUtils.assertArray(json);
        List<String[]> paths = new ArrayList<>();

        Iterator iter = array.iterator();
        while (iter.hasNext()) {
            Object obj = iter.next();
            if (obj instanceof JsonArray)
                paths.add(JsonUtils.toList((JsonArray)obj).toArray(new String[0]));
        }

        return new IndexEntry(paths);
    }
}
