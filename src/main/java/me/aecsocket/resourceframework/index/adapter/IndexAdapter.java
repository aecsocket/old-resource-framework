package me.aecsocket.resourceframework.index.adapter;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import me.aecsocket.pluginutils.json.JsonAdapter;
import me.aecsocket.pluginutils.json.JsonUtils;
import me.aecsocket.resourceframework.index.Index;
import me.aecsocket.resourceframework.index.IndexEntry;

import java.lang.reflect.Type;
import java.util.Map;

public class IndexAdapter implements JsonAdapter<Index> {
    @Override
    public JsonElement serialize(Index object, Type type, JsonSerializationContext context) {
        return context.serialize(object.getEntries());
    }

    @Override
    public Index deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = JsonUtils.assertObject(json);
        return new Index(context.deserialize(jsonObject, new TypeToken<Map<String, IndexEntry>>(){}.getType()));
    }
}
