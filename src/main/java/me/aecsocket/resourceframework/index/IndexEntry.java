package me.aecsocket.resourceframework.index;

import org.bukkit.plugin.Plugin;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static me.aecsocket.resourceframework.ResourceFramework.getResource;
import static me.aecsocket.resourceframework.ResourceFramework.getStringResource;
import static me.aecsocket.resourceframework.ResourceFramework.joinPath;

/** A list of file paths of an index resource. */
public class IndexEntry {
    /** A {@link List} of an array of {@link String} file paths. */
    private List<String[]> paths;

    public IndexEntry(List<String[]> paths) {
        this.paths = paths;
    }

    public IndexEntry() {
        this.paths = new ArrayList<>();
    }

    /** Gets a {@link List} of an array of {@link String} file paths.
     * @return A {@link List} of an array of {@link String} file paths.
     */
    public List<String[]> getPaths() { return paths; }

    /** Adds an array of {@link String} file paths.
     * @param path An array of {@link String} file paths.
     */
    public void addPath(String... path) { paths.add(path); }

    /** Gets a {@link List} of resource {@link InputStream}s from a {@link Plugin}.
     * @param plugin The {@link Plugin}.
     * @return The {@link List} of resource {@link InputStream}s.
     */
    public List<InputStream> getResources(Plugin plugin) {
        List<InputStream> resources = new ArrayList<>();
        for (String[] path : paths)
            resources.add(getResource(plugin, joinPath(path)));
        return resources;
    }

    /** Gets a {@link List} of resource {@link String}s from a {@link Plugin}.
     * @param plugin The {@link Plugin}.
     * @return The {@link List} of resource {@link String}s.
     */
    public List<String> getStringResources(Plugin plugin) {
        List<String> resources = new ArrayList<>();
        for (String[] path : paths)
            resources.add(getStringResource(plugin, joinPath(path)));
        return resources;
    }

    @Override
    public String toString() {
        List<String> out = new ArrayList<>();
        for (String[] path : paths)
            out.add(String.format("[%s]", String.join("/", path)));
        return String.format("[%s]", String.join(", ", out));
    }
}
