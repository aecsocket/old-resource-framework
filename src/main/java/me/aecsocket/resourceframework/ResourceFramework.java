package me.aecsocket.resourceframework;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.aecsocket.resourceframework.index.Index;
import me.aecsocket.resourceframework.index.IndexEntry;
import me.aecsocket.resourceframework.index.adapter.IndexAdapter;
import me.aecsocket.resourceframework.index.adapter.IndexEntryAdapter;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.IOUtils;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

/** The main plugin class. */
public class ResourceFramework extends JavaPlugin {
    /** A helper class to use for your own plugin. */
    public static class PluginToken {
        /** The associated {@link Plugin}. */
        private Plugin plugin;

        public PluginToken(Plugin plugin) {
            this.plugin = plugin;
        }

        /** Gets the associated {@link Plugin}.
         @return The associated {@link Plugin}.
         */
        public Plugin getPlugin() { return plugin; }

        /** See {@link ResourceFramework#getResource(Plugin, String)} */
        public InputStream getResource(String path) { return ResourceFramework.getResource(plugin, path); }

        /** See {@link ResourceFramework#getStringResource(Plugin, String)} */
        public String getStringResource(String path) { return ResourceFramework.getStringResource(plugin, path); }

        /** See {@link ResourceFramework#getData(Plugin, String)} */
        public InputStream getData(String path) { return ResourceFramework.getData(plugin, path); }

        /** See {@link ResourceFramework#getStringData(Plugin, String)} */
        public String getStringData(String path) { return ResourceFramework.getStringData(plugin, path); }

        /** See {@link ResourceFramework#saveData(Plugin, InputStream, String)} */
        public void saveData(InputStream stream, String path) { ResourceFramework.saveData(plugin, stream, path); }

        /** See {@link ResourceFramework#saveStringData(Plugin, String, String)} */
        public void saveStringData(String data, String path) { ResourceFramework.saveStringData(plugin, data, path); }

        /** See {@link ResourceFramework#getIndex(Plugin)} */
        public Index getIndex() { return ResourceFramework.getIndex(plugin); }
    }

    /** An instance of this plugin. */
    private static ResourceFramework instance;
    /** The {@link Gson} this instance uses to get {@link Index}es. */
    private static Gson gson;

    /** Gets an instance of this plugin.
     * @return An instance of this plugin.
     */
    public static ResourceFramework getInstance() { return instance; }

    @Override
    public void onEnable() {
        instance = this;
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(IndexEntry.class, new IndexEntryAdapter())
                .registerTypeAdapter(Index.class, new IndexAdapter())
                .create();
    }

    private static Path getDataFolderPath(Plugin plugin, String path) {
        return Paths.get(plugin.getDataFolder().getAbsolutePath(), path);
    }

    private static String streamToString(InputStream stream) {
        if (stream != null) {
            try {
                return IOUtils.toString(stream, StandardCharsets.UTF_8);
            } catch (IOException ignore) {}
        }
        return null;
    }

    /** Joins an array of {@link String}s into a {@link String}s path.
     * @param paths The array of {@link String}s.
     * @return The {@link String} path.
     */
    public static String joinPath(String... paths) { return String.join("/", paths); }

    /** Gets a {@link Plugin}'s resource.
     * @param plugin The {@link Plugin}.
     * @param path The path.
     * @return The {@link InputStream} of the resource.
     */
    public static InputStream getResource(Plugin plugin, String path) {
        return plugin.getResource(path);
    }

    /** Gets a {@link Plugin}'s resource as a {@link String}.
     * @param plugin The {@link Plugin}.
     * @param path The path.
     * @return The {@link String} of the resource.
     */
    public static String getStringResource(Plugin plugin, String path) {
        return streamToString(getResource(plugin, path));
    }

    /** Gets a {@link Plugin}'s data file.
     * @param plugin The {@link Plugin}.
     * @param path The path.
     * @return The {@link InputStream} of the data file.
     */
    public static InputStream getData(Plugin plugin, String path) {
        try {
            return new FileInputStream(getDataFolderPath(plugin, path).toFile());
        } catch (IOException ignore) {}
        return null;
    }

    /** Gets a {@link Plugin}'s data file as a {@link String}.
     * @param plugin The {@link Plugin}.
     * @param path The path.
     * @return The {@link String} of the data file.
     */
    public static String getStringData(Plugin plugin, String path) {
        return streamToString(getData(plugin, path));
    }

    /** Saves data to a {@link Plugin}'s data file.
     * @param plugin The {@link Plugin}.
     * @param stream The {@link InputStream} of data.
     * @param path The path.
     */
    public static void saveData(Plugin plugin, InputStream stream, String path) {
        Path jPath = getDataFolderPath(plugin, path);
        try {
            jPath.getParent().toFile().mkdirs();
            FileUtils.copyInputStreamToFile(stream, jPath.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Saves string data to a {@link Plugin}'s data file.
     * @param plugin The {@link Plugin}.
     * @param data The {@link String} of data.
     * @param path The path.
     */
    public static void saveStringData(Plugin plugin, String data, String path) {
        saveData(plugin, new ByteArrayInputStream(data.getBytes()), path);
    }

    /** Gets a {@link Plugin}'s {@link Index}.
     * @param plugin The {@link Plugin}.
     * @return The {@link Index}.
     */
    public static Index getIndex(Plugin plugin) {
        String json = getStringResource(plugin, Index.INDEX_PATH);
        if (json != null)
            return gson.fromJson(json, Index.class);
        return null;
    }
}
